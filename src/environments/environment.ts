// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyBc9z1-Q-SnnM7kBWrAk1PCkRmY2E3hvLs",
    authDomain: "hello-jcemg.firebaseapp.com",
    databaseURL: "https://hello-jcemg.firebaseio.com",
    projectId: "hello-jcemg",
    storageBucket: "hello-jcemg.appspot.com",
    messagingSenderId: "441778168114",
    appId: "1:441778168114:web:64c9c83101cfedaf57b72b"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
